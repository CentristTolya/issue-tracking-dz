//
// Created by Test-notebook on 5/28/2021.
//

#include "InterfaceScreen.h"

using namespace CoolichWithYou;

int SubjectList() {
    system("cls");
    std::cout << "Открыть список предметов\n\n";
    if(login == "admin") {

        CMenuItem items[6]{
                CMenuItem{"Отобразить список предметов", printSubjectList},
                CMenuItem{"Сортировать по названию", sortSubjectByName},
                CMenuItem{"Добавить новый элемент", addNewSubject},
                CMenuItem{"Редактировать предмет по id", editSubject},
                CMenuItem{"Удалить предмет по id", deleteSubject},
                CMenuItem{"Вернуться назад", runMenu}
        };

        CMenu menu("Subject menu", items, 6);

        menu.runCommand();
    }
    else{
        CMenuItem items[3]{
                CMenuItem{"Отобразить список предметов", printSubjectList},
                CMenuItem{"Сортировать по названию", sortSubjectByName},
                CMenuItem{"Вернуться назад", runMenu}
        };

        CMenu menu("Subject menu", items, 3);

        menu.runCommand();
    }
    return 1;
}

int authorizathion(){
    login = "-";
    while(login == "-") {
        auth();
    }

    runMenu();
}

int runMenu(){
    if(login == "admin"){
        adminRun();
    }
    else{
        usualRun();
    }
}

void adminRun(){
    system("cls");
    CMenuItem items[2]{CMenuItem{"Открыть список предметов", SubjectList},
                       CMenuItem{"Разлогиниться", authorizathion}};
    CMenu menu("My console menu", items, 2);

    menu.runCommand();
}

void usualRun(){
    system("cls");
    CMenuItem items[2]{CMenuItem{"Открыть список предметов", SubjectList},
                       CMenuItem{"Разлогиниться", authorizathion}};
    CMenu menu("My console menu", items, 2);

    menu.runCommand();
}