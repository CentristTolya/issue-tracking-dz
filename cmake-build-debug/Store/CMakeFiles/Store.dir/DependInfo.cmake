# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "C:/Users/Test-notebook/Desktop/opi-dz/Store/State.cpp" "C:/Users/Test-notebook/Desktop/opi-dz/cmake-build-debug/Store/CMakeFiles/Store.dir/State.cpp.obj"
  "C:/Users/Test-notebook/Desktop/opi-dz/Store/Store.cpp" "C:/Users/Test-notebook/Desktop/opi-dz/cmake-build-debug/Store/CMakeFiles/Store.dir/Store.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../Models"
  "../Containers"
  "../MyConMenu"
  "../Store"
  "../Screens"
  "../Store/../Screens"
  "../Store/../Models"
  "../Store/../Containers"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
